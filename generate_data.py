#!/usr/bin/env python3
from shared import Aggregate
from classic import HERBS as CLASSIC_HERBS, ORES as CLASSIC_ORES, TREASURES as CLASSIC_TREASURES, FISHES as CLASSIC_FISHES, GASES as CLASSIC_GASES, ARCHAEOLOGY as CLASSIC_ARCHAEOLOGY
from era import HERBS as ERA_HERBS, ORES as ERA_ORES, TREASURES as ERA_TREASURES, FISHES as ERA_FISHES


if __name__ == '__main__':
    # Classic
    with open("GatherMate2_Data/Classic/HerbalismData.lua", "w") as file:
        print(Aggregate("Herb", CLASSIC_HERBS), file=file)
    with open("GatherMate2_Data/Classic/MiningData.lua", "w") as file:
        print(Aggregate("Mine", CLASSIC_ORES), file=file)
    with open("GatherMate2_Data/Classic/TreasureData.lua", "w") as file:
        print(Aggregate("Treasure", CLASSIC_TREASURES), file=file)
    with open("GatherMate2_Data/Classic/FishData.lua", "w") as file:
        print(Aggregate("Fish", CLASSIC_FISHES), file=file)
    with open("GatherMate2_Data/Classic/GasData.lua", "w") as file:
        print(Aggregate("Gas", CLASSIC_GASES), file=file)
    with open("GatherMate2_Data/Classic/ArchaeologyData.lua", "w") as file:
        print(Aggregate("Archaeology", CLASSIC_ARCHAEOLOGY), file=file)

    # Era
    with open("GatherMate2_Data/Era/HerbalismData.lua", "w") as file:
        print(Aggregate("Herb", ERA_HERBS), file=file)
    with open("GatherMate2_Data/Era/MiningData.lua", "w") as file:
        print(Aggregate("Mine", ERA_ORES), file=file)
    with open("GatherMate2_Data/Era/TreasureData.lua", "w") as file:
        print(Aggregate("Treasure", ERA_TREASURES), file=file)
    with open("GatherMate2_Data/Era/FishData.lua", "w") as file:
        print(Aggregate("Fish", ERA_FISHES), file=file)
