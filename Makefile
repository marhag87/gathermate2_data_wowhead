generate:
	python3 generate_data.py && \
	sed -i "s/## Version: v.*/## Version: v$$(date +%F)/" GatherMate2_Data/GatherMate2_Data_Cata.toc && \
	awk -F'"' '/GatherMateData.generatedVersion/ {$$2=$$2+1}1' OFS='"' GatherMate2_Data/Classic/GatherMateData.lua > tmp && mv tmp GatherMate2_Data/Classic/GatherMateData.lua && \
	sed -i "s/## Version: v.*/## Version: v$$(date +%F)/" GatherMate2_Data/GatherMate2_Data_Classic.toc && \
	awk -F'"' '/GatherMateData.generatedVersion/ {$$2=$$2+1}1' OFS='"' GatherMate2_Data/Era/GatherMateData.lua > tmp && mv tmp GatherMate2_Data/Era/GatherMateData.lua

commit:
	git commit -a -m "Updates for $$(grep '## Version' GatherMate2_Data/GatherMate2_Data_Classic.toc | cut -d'v' -f2)"

zip:
	zip -r gathermate2_data_wowhead_classic_v$$(grep "## Version" GatherMate2_Data/GatherMate2_Data_Classic.toc | cut -d"v" -f2).zip GatherMate2_Data/

update:
	$(MAKE) generate && \
	$(MAKE) commit && \
	$(MAKE) zip

clean:
	git clean -ffxd -e .idea
